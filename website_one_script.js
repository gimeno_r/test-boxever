var urlApi = 'https://api.capturedata.ie'; //URL to send the data
var targetClass = 'flight-header flight-header--selected'; //Start point in the html
var flights = []; //Array that will contain the collected data

/***** Start process to collect data *****/

var selectedFlights = document.getElementsByClassName(targetClass);
var rootNode = {};
var flight = {};

//Iterate through the elements that the targeted class has
for (var i = 0; i < selectedFlights.length; i++) {
    rootNode = selectedFlights.item(i);
    if (rootNode) {
        flight = {};
        recurseDomChildren(rootNode, flight); //Start searching the flight information
        flights.push(flight);  
    }
}

console.log(flights);
sendData(flights, urlApi); //as the URL doesn't exist an error will show up on the console

/***** Functions to perform the search *****/


/**
 * Goes from the start point through its children recursively
 */
function recurseDomChildren(start, flight) {
    if (start.childNodes) {
        var nodes = start.childNodes;
        loopNodeChildren(nodes, flight);
    }
}

/**
 * Loops through the given nodes
 */
function loopNodeChildren(nodes, flight) {
    var node;
    for (var i = 0; i < nodes.length; i++) {
        node = nodes[i];

        //Check if the node contains information
        if (node.nodeType === 3 && node.data) {
            updateFlight(node, flight);
        } 
        
        //Check the children
        if (node.childNodes) {
            recurseDomChildren(node, flight);
        }
    }
}

/**
 * Updates data model base on the style classes
 */
function updateFlight(node, flight) {
    switch (node.parentNode.className) {
        case 'hide-mobile flight-header__details-time':
            flight.date = node.data;
            break;
        case 'flight-header__connection-time-label':
            flight.duration = node.data;
            break;
        case 'flight-number':
            flight.number = node.data;
            break;
        case 'start-time':
            flight.departTime = node.data;
            break;
        case 'start-time__departure':
            flight.departLocation = node.data;
            break;
        case 'end-time':
            flight.arrivalTime = node.data;
            break;
        case 'end-time__destination':
            flight.arrivalLocation = node.data;
            break;
    }
}

/**
 * Sends the data to the given URL
 */
function sendData(data, url) {
    var request = new XMLHttpRequest();
    var formData  = new FormData();

    for(name in data) {
        formData.append(name, data[name]);
    }

    request.open('POST', url);

    request.send(formData);
}