var urlApi = 'https://api.capturedata.ie'; //URL to send the data
//Set airlines and their start points to get the data
var ryanair = {name: 'ryanair', classes: ['flight-header flight-header--selected']};
var aircanada = {name: 'aircanada', ids: ['flight_selected_section_0', 'flight_selected_section_1']};
var websites = [ryanair, aircanada];
var flights = [];

/**** Start process to collect data ****/

websites.forEach(function(website) {
    if (website.hasOwnProperty('classes')) {
        website.classes.forEach(function(className) {
             searchByClass(className, flights, website.name);
        });
    } else  if (website.hasOwnProperty('ids')) {
        website.ids.forEach(function(id) {
             searchById(id, flights, website.name);
        });
    }
});

console.log(flights);
sendData(flights, urlApi); //as the URL doesn't exist an error will show up on the console

/***** Functions to perform the search ******/

/**
 * Starts a search using an id as a target element to start
 */
function searchById(id, flights, airlineName) {
    startSearch(document.getElementById(id), airlineName, flights)
}

/**
 * Starts a search using a style class as a target element to start
 */
function searchByClass(className, flights, airlineName) {
    var selectedFlights = document.getElementsByClassName(className);
    for (var i = 0; i < selectedFlights.length; i++) {
        startSearch(selectedFlights.item(i), airlineName, flights);
    }
}

function startSearch(rootNode, airlineName, flights) {
    if (rootNode) {
        var flight = {airline: airlineName};
        recurseDomChildren(rootNode, flight);
        flights.push(flight);  
    }
}

/**
 * Goes from the start point through its children recursively
 */
function recurseDomChildren(start, flight) {
    var nodes;
    if (start.childNodes) {
        nodes = start.childNodes;
        loopNodeChildren(nodes, flight);
    }
}

/**
 * Loops through the given nodes
 */
function loopNodeChildren(nodes, flight) {
    var node;
    for (var i=0; i < nodes.length; i++) {        
        node = nodes[i];   
        //Check if the node contains information
        if (node.nodeType === 3 && node.data) {
            updateFlight(node, flight);
        } 
        
        //Check the children
        if (node.childNodes) {
            recurseDomChildren(node, flight);
        }
    }
}

function updateFlight (node, flight) {
    if (flight.airline === ryanair.name) {
        updateRyanairFlight(node, flight);
    } else if (flight.airline === aircanada.name) {
        updateCanadaFlight(node, flight);
    } 
}

/**
 * Updates data model base on the style classes in ryanair website
 */
function updateRyanairFlight(node, flight) {
    switch (node.parentNode.className) {
        case 'hide-mobile flight-header__details-time':
            flight.date = node.data;
            break;
        case 'flight-header__connection-time-label':
            flight.duration = node.data;
            break;
        case 'flight-number':
            flight.number = node.data;
            break;
        case 'start-time':
            flight.departTime = node.data;
            break;
        case 'start-time__departure':
            flight.departLocation = node.data;
            break;
        case 'end-time':
            flight.arrivalTime = node.data;
            break;
        case 'end-time__destination':
            flight.arrivalLocation = node.data;
            break;
    }
}

/**
 * Updates data model base on the style classes in air canada website
 */
function updateCanadaFlight(node, flight) {
     switch (node.parentNode.className) {
        case 'flight_selected_day':
            var date = $(node).closest('div.col-lg-2.col-md-2.col-sm-2.hidden-xs.flight_selected_date');
            if (date.length > 0) {           
                var day = node.data;            
                date = date[0].childNodes[6].lastChild.data;
                flight.date = day + ' ' + date;   
            }
            break;
        case 'flight_duration font-face-sb':
            var hours = node.parentNode.childNodes[0].lastChild.data;
            var minutes = node.parentNode.childNodes[2].lastChild.data;
            flight.duration = hours + 'hr ' + minutes + 'mins';
            break;
        case 'ac_flight_num':
            flight.number = node.data;
            break;
        case 'flight_selected_time font_face_l':
            var departure = $(node).closest('div.col-lg-4.col-md-4.col-sm-4.col-xs-12.departure-selected-info');
            if (departure.length > 0) {
                flight.departTime = node.data;   
            } else {
                flight.arrivalTime = node.data;
            }
            break;
        case 'flight_selected_city_name font-face-sb':           
            var departure = $(node).closest('div.col-lg-4.col-md-4.col-sm-4.col-xs-12.departure-selected-info');
            if (departure.length > 0) {
                flight.departLocation = node.data;   
            } else {
                flight.arrivalLocation = node.data;
            }
            break;
     }
}

/**
 * Sends the data to the given URL
 */
function sendData(data, url) {
    var request = new XMLHttpRequest();
    var formData  = new FormData();

    for(name in data) {
        formData.append(name, data[name]);
    }

    request.open('POST', url);

    request.send(formData);
}